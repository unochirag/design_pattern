//
//  ViewController.swift
//  design_pattern
//
//  Created by Chirag on 25/03/20.
//  Copyright © 2020 Unoapp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Fectory Method Pattern : - ")
        self.performFectoryMethodPattern()
        
        print("Fectory Method With Singleton Pattern : - ")
        self.perfomeFectoryMethodWithSingletonPattern()
        
    }
    
    func performFectoryMethodPattern(){
        
        var chirag = Person() //person with empty details
        let personBuilder = PersonBuilder(person: chirag)
        
        chirag = personBuilder
            .personalInfo
                .nameIs("Chirag Vegad")
                .genderIs("Male")
                .bornOn("16th Mar 1991")
                .bornAt("Gujarat, India")
                .havingHeight("5.5 ft")
                .havingWeight("78 kg")
            .contacts
                .hasPhone("+91 90333-764202")
                .hasEmail("chirag@unoindia.co")
            .lives
                .at("Kumbharwad")
                .inCity("Vyara")
                .withZipCode("394650")
            .build()
        
        //use of Person object
        print("\(chirag.name) has contact number \(chirag.phone) and email \(chirag.email)")
        
        //later on when we have company details ready for the person
        chirag = personBuilder
            .works
                .asA("Software Developer")
                .inCompany("Fortune 500")
                .hasAnnualEarning("45,000 USD")
            .build()
        
        //use of Person object with update info
        print("\(chirag.name) works in \(chirag.companyName) compay as a \(chirag.designation).")
    }
    func perfomeFectoryMethodWithSingletonPattern(){
        
        let personBuilder = PersonBuilder(person: Person.shared)
        Person.shared = personBuilder
            .personalInfo
                .nameIs("Chirag Vegad")
                .genderIs("Male")
                .bornOn("16th Mar 1991")
                .bornAt("Gujarat, India")
                .havingHeight("5.5 ft")
                .havingWeight("78 kg")
            .contacts
                .hasPhone("+91 90333-764202")
                .hasEmail("chirag@unoindia.co")
            .lives
                .at("Kumbharwad")
                .inCity("Vyara")
                .withZipCode("394650")
            .build()
        
        //use of Person object
        print("\(Person.shared.name) has contact number \(Person.shared.phone) and email \(Person.shared.email)")
        
        //later on when we have company details ready for the person
        Person.shared = personBuilder
            .works
                .asA("Software Developer")
                .inCompany("Fortune 500")
                .hasAnnualEarning("45,000 USD")
            .build()
        
        //use of Person object with update info
        print("\(Person.shared.name) works in \(Person.shared.companyName) compay as a \(Person.shared.designation).")
    }
}

